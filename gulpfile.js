var gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    browserSync = require('browser-sync'),
    path = {
        js: 'src/app/**/*.js',
        scss: 'src/assets/styles/**/*.scss',
        html: 'src/**/*.html',
        mainScss: 'src/assets/styles/main.scss'
    };

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: ['src', '.tmp'],
            routes: {
                '/bower_components': 'bower_components'
            }
        },
        open: false,
        notify: false
    });
});

gulp.task('serve:dev', ['jshint', 'styles', 'browser-sync'], function() {
    gulp.watch(path.scss, ['styles']);
    gulp.watch(path.js, ['jshint']);
    gulp.watch(path.html, ['reload']);
});

gulp.task('reload', function() {
    browserSync.reload();
});

gulp.task('styles', function() {
    return $.rubySass(path.mainScss, {
            sourcemap: true,
            style: 'expanded',
            precision: 10,
            loadPath: [
                'bower_components/gridle/sass'
            ]
        })
        .on('error', function(error) {
            console.error('SASS Error!', error.message);
        })
        .pipe($.autoprefixer())
        .pipe(gulp.dest('.tmp/styles'))
        .pipe($.if(browserSync.active, browserSync.reload({stream: true})));
});

gulp.task('jshint', function() {
    return gulp.src(path.js)
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.if(!browserSync.active, $.jshint.reporter('fail')))
        .pipe($.if(browserSync.active, browserSync.reload({stream: true})));
});
