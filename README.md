AngularJS Exam
==============

Dear developer, the goal of this exam is to know a little more about your programming skills. Here at Speed to Contact we value a code with good quality, that's easy to understand, extend, maintain and to re-use.

The task is simple: play with the LinkedIn or Facebook API, preferably LinkedIn (if you want to do both, awesome). You will have to develop an application using AngularJS that will consume some public resources from a LinkedIn user contacts through its API, and present it in a simple way but well-structured, as a contact list. Here are some suggestions of what you can do:

- List them in a very creative way
- Option to filter contacts
- Show detailed info when click on a contact
- Option to send a message (can be just the front-end)
- ... use your imagination

LinkedIn API for contacts: [https://api.linkedin.com/v1/people/~/connections?modified=new]()
Here's a link for you to play with the API: [https://apigee.com/console/linkedin]() (you'll need a LinkedIn account to authenticate through OAuth2)

To get started, just fork this repository, implement and then make a pull request. Or if you don't want to send a PR, just send the link of your fork for us by e-mail (brayanr@speedtocontact.com). List the features you developed in the README and how to run the app.

It is very important that you show us your knowledge regarding good programming practices, testing, patterns and even the bad practices. Therefore, take these points into consideration when choosing and implementing your features.

Besides coding skills, there are two things that aren't strictly required but would be nice to see you using:

- a CSS pre-processor of your choice
- a build system like Gulp or Grunt
- Any other technologies you find interesting

And if you feel really inspired, tell us how did you make your decisions regarding the tools you've chosen.

We wish you good luck, and may the Quality be with you!
