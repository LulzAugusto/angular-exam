(function() {
    'use strict';

    angular
        .module('app.core', [
        	/*
        	 * Angular modules
        	 */

        	/*
        	 * Reusable cross app modules
        	 */

            /*
             * 3rd party modules
             */
            'ui.router'
        ]);
})();
