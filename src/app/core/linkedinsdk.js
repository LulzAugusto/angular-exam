(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('LinkedinSDK', LinkedinSDK);

    LinkedinSDK.$inject = ['IN'];

    /* @ngInject */
    function LinkedinSDK(IN) {
        var service = {
            get: get,
            getConnections: getConnections
        };

        return service;

        function get(uri, fields, params, callback) {
            var url = uri + serializeToFields(fields) + serializeToParams(params);
            IN.API.Raw()
                .url(url)
                .method('GET')
                .body()
                .result(callback);
        }

        function getConnections(callback) {
            IN.API.Connections('me')
                .fields(
                    'first-name', 'last-name', 'summary', 'industry',
                    'positions', 'public-profile-url', 'picture-urls::(original)',
                    'headline', 'picture-url', 'email-address', 'formatted-name',
                    'date-of-birth'
                )
                .params({'count':50})
                .result(callback);
        }

        function serializeToParams(object) {
            var str = [];
            for(var property in object) {
                if (object.hasOwnProperty(property)) {
                   str.push(encodeURIComponent(property) + '=' + encodeURIComponent(object[property]));
                }
            }
            return '?' + str.join('&');
        }

        function serializeToFields(fields) {
            return ':(' + fields.join(',') + ')';
        }
    }
})();
