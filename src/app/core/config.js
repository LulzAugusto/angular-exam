(function() {
	'use strict';

	angular
		.module('app.core')
		.config(config);

	config.$inject = ['$locationProvider', '$httpProvider'];

	function config($locationProvider, $httpProvider) {
		$locationProvider.html5Mode(true);
	}
})();
