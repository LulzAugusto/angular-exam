(function() {
	'use strict';

	angular
		.module('app.contacts')
		.controller('ContactsController', ContactsController);

    ContactsController.$inject = ['Contacts', '$scope'];

	/* @ngInject */
	function ContactsController(Contacts, $scope) {
        var vm = this;

        vm.list = [];
        vm.select = select;
        vm.activeContact = false;

        activate();

        function activate() {
            getContacts();
        }

        function select(contact) {
            vm.activeContact = false;
            vm.activeContact = contact;
        }

        /**
         * Will populate our contacts list and accepts a callback function
         * (Simulating $http's promise behaviour)
         * @param  {Function} callback
         * @return {undefined}
         */
        function getContacts(callback) {
            Contacts.getConnections(function(contacts) {
                vm.list = contacts;
                $scope.$apply();

                if (typeof callback === 'function') {
                    callback();
                }
            });
        }
	}
})();
