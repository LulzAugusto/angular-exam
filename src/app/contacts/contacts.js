(function() {
    'use strict';

    angular
        .module('app.contacts')
        .factory('Contacts', Contacts);

    Contacts.$inject = ['LinkedinSDK'];

    /* @ngInject */
    function Contacts(LinkedinSDK) {
        var fields = [
            'first-name', 'last-name', 'summary', 'industry',
            'positions', 'public-profile-url', 'picture-urls::(original)',
            'headline', 'picture-url', 'email-address', 'formatted-name',
            'date-of-birth'
        ];

        var service = {
            getConnections: getConnections
        };
        return service;

        function getConnections(callback) {
            LinkedinSDK.getConnections(function(response) {
                var contacts = filterInvalidContacts(response.values);
                if (typeof callback === 'function') {
                    callback(contacts.sort(compareContacts));
                }
            });
        }

        function filterInvalidContacts(contacts) {
            angular.forEach(contacts, function(contact, key) {
                if (contact.firstName === 'private') {
                    contacts.splice(key, 1);
                }
            });
            return contacts;
        }

        function compareContacts(a, b) {
            if (a.firstName < b.firstName) {
                return -1;
            }

            if (a.firstName > b.firstName) {
                return 1;
            }

            return 0;
        }
    }
})();
