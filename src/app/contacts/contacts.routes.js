(function() {
	'use strict';

	angular
		.module('app.contacts')
		.config(config);

	config.$inject = ['$stateProvider'];

	/* @ngInject */
	function config($stateProvider) {
		$stateProvider
			.state('contacts', {
				url: '/contacts',
				templateUrl: 'app/contacts/contacts.html',
				controller: 'ContactsController',
				controllerAs: 'contacts'
			});
	}
})();
