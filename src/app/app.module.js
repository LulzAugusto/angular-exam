(function() {
	'use strict';

	angular
		.module('app', [
			/*
			 * Shared Modules
			 */
			'app.core',

			/*
			 * Feature Modules
			 */
			'app.auth',
			'app.contacts'
		]);
})();
