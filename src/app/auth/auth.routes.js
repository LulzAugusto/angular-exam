(function() {
    'use strict';

    angular
        .module('app.auth')
        .config(config);

    config.$inject = ['$stateProvider'];

    /* @ngInject */
    function config($stateProvider) {
        $stateProvider
            .state('auth', {
                url: '/',
                templateUrl: 'app/auth/auth.html',
                controller: 'AuthController',
                controllerAs: 'auth'
            });
    }
})();
