(function() {
    'use strict';

    angular
        .module('app.auth')
        .controller('AuthController', AuthController);

    AuthController.$inject = ['$state', 'IN'];

    /* @ngInject */
    function AuthController($state, IN) {
        var vm = this;

        vm.login = login;

        function login() {
            IN.User.authorize(function() {
                $state.go('contacts');
            });
        }
    }
})();
